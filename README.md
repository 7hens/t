# tiny-url

This is a url shortener that can be entirely hosted on Gitlab pages.

This project is inspired by the repo [nelsontky/gh-pages-url-shortener](https://github.com/nelsontky/gh-pages-url-shortener) which is a url shortener with github pages.

## Quick Start

1. To add a new short link, [add an issue](../../issues) with the title being the link you want to shorten (including `http(s)://`)
2. The newly created short url can be accessed via `xxx.gitlab.io/tiny-url/{issue_number}`
3. If you want to get a more shorter url, you can copy the [`404.html`](./404.html) file to your another project named `xxx.gitlab.io`, and you can access the url via `xxx.gitlab.io/{issue_number}`
